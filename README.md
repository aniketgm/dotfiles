# Table of Contents

- [Dotfile Managment](#dotfile-managment)
  - [Overview](#overview)
  - [Setup start](#setup-start)
  - [Script actions](#script-actions)

## Dotfile Managment

### Overview

For managing the dotfiles I'm using the [Rotz](https://github.com/volllly/rotz) tool.

Best part of Rotz is it can be used on any system (Linux/Mac and even Windows as well).
Another thing is that you can not only configure but also install softwares using Rotz.
One just has to maintain config files for it. Supports yaml, toml and json formats for config files.

### Setup start

I work on Windows and Linux (mostly Ubuntu), so I have created two script files, *setup.ps1* for Windows and *setup* for Linux.

### Script actions

#### Windows setup:

1. Install Git
```sh
winget install --id Git.Git --exact --source winget
```

2. Install Rotz from Powershell prompt and restart the system so that Git and Rotz are installed properly
```sh
Invoke-RestMethod volllly.github.io/rotz/install.ps1 | Invoke-Expression
Restart-Computer
```

3. Once restarted, clone this dotfiles repo using Rotz.
```sh
cd ~
rotz clone https://gitlab.com/aniketgm/dotfiles.git
# This will clone the dotfiles repo into the home directory as ~/.dotfiles
```

4. Now, either run the *setup.ps1* OR perform manual install and configuration.
  - To run *setup.ps1*, do the following from elevated powershell (as admin):
  ```sh
  cd ~/.dotfiles    # Assuming you cloned the dotfiles in the $HOME directory
  .\setup.ps1
  ```

  - For manually installing packages, refer the commands inside *setup.ps1*

5. The *setup.ps1* will install the following packages and link configs (check inside the *setup.ps1* for more details):
  - Installation of packages/softwares
    - Microsoft VCRedist 2015-2022 (For running various Windows apps | Required)
    - PowerShell 7 (Latest PowerShell with cool features | Required)
    - Windows Terminal (Terminal tool provided by Microsoft | Required, mostly required for quake mode)
    - Winget (Microsofts own package manager tool | Required)
    - Scoop (CLI installer for Windows | Optional, if winget is used)
    - Once Scoop in installed, then, install packages like 7zip, fzf, ripgrep, lazygit, etc..
      - Check out the `dot.yml` file in `extras/scoop/packages` folder for more details.
    - Other important packages
      - LazyVim (NeoVim distribution, IDE for development | Required)
      - Wezterm (Cross-platform terminal emulator and multiplexer | Required)
  - Link configurations to their respective locations.
    - Check out the [config.yaml](https://gitlab.com/aniketgm/dotfiles/-/blob/main/rotz/config.yaml) under the rotz folder

#### Linux setup:

1. Install Git
```sh
sudo apt install git
```

2. Install Rotz and restart the system for changes to take effect
```sh
curl -fsSL volllly.github.io/rotz/install.sh | sh
reboot
```

3. Once restarted, clone the repo
```sh
cd ~
rotz clone https://gitlab.com/aniketgm/dotfiles.git
# This will clone the dotfiles repo into the home directory as ~/.dotfiles
```

5. Now, either run *setup.ps1* OR perform manual install and configuration.
  - To run *setup*, do the following from an elevated bash prompt
  ```sh
  cd ~/.dotfiles    # Assuming you cloned the dotfiles in the $HOME directory
  chmod +x ./setup  # Set execute permissions if not set.
  ./setup
# This will place the respective configrations in their places after installation.
  ```

  - The *setup* script will perform the following installations and configrations
