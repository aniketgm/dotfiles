--              __________________
--          /\  \   __           /  /\    /\           Author      : Aniket Meshram [AniGMe]
--         /  \  \  \         __/  /  \  /  \          Description : This is a wezterm configuration file
--        /    \  \       _____   /    \/    \                       similar to the tmux on Linux. This launches
--       /  /\  \  \     /    /  /            \                      a multiplexer environment to run ssh sessions,
--      /        \  \        /  /      \/      \                     terminals and terminal-programs.
--     /          \  \      /  /                \
--    /            \  \    /  /                  \     Github Repo : https://github.com/aniketgm/Dotfiles
--   /              \  \  /  /                    \
--  /__            __\  \/  /__                  __\
--

-- Include main wezterm object
local wz = require("wezterm")

-- Include local setups
local cl = require("custom.wez-colors")
local km = require("config.wez-keymaps")
local ui = require("config.wez-ui")

require("events.wez-right-status").setup()
require("events.wez-tab-title").setup()
require("events.wez-gui-startup").setup()

local tabbar = require("custom.wez-tabbar")

-- Set main config variable
local cf = {}
if wz.config_builder then
  cf = wz.config_builder()
end

-- Tab bar: tabline plugin config customization
-- Link: https://github.com/michaelbrusegard/tabline.wez
-- tabbar.tabline.apply_to_config(cf)

-- My custom tabline
tabbar.apply_to_config(cf)

-- Add UI config
ui.add_to_config(cf)

-- Add keymaps config
km.add_to_config(cf)

return cf
