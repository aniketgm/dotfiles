local M = {}

-- Unicode chars of interest
local HALF_CIRCLE = {}
HALF_CIRCLE.RIGHT_FILLED = utf8.char(0xE0B4) -- ""
HALF_CIRCLE.RIGHT_THIN = utf8.char(0xE0B5) -- ""
HALF_CIRCLE.LEFT_FILLED = utf8.char(0xE0B6) -- ""
HALF_CIRCLE.LEFT_THIN = utf8.char(0xE0B7) -- ""

local ARROW = {}
ARROW.RIGHT_FILLED = utf8.char(0xE0B0) -- ""
ARROW.RIGHT_THIN = utf8.char(0xE0B1) -- ""
ARROW.LEFT_FILLED = utf8.char(0xE0B2) -- ""
ARROW.LEFT_THIN = utf8.char(0xE0B3) -- ""

M.SOLID_BOX_FILLED = utf8.char(0x258E) -- "▎"

M.SEPARATOR = HALF_CIRCLE

local COLORS_SET_1 = {}
COLORS_SET_1.ACTIVE_FG = "#191F26"
COLORS_SET_1.ACTIVE_BG = "#FFBFC5"
COLORS_SET_1.NORMAL_BG = "#3E4359"
COLORS_SET_1.NORMAL_FG = "#ff99cc"
COLORS_SET_1.HOVER_FG = "#CC66FF"
COLORS_SET_1.HOVER_BG = "#1A1A00"

COLORS_SET_1.WKSP = "#99ffff"
COLORS_SET_1.TIME = "#b3ffb3"
COLORS_SET_1.DATE = "#ffff99"
COLORS_SET_1.BATT = "#fc7ceb"

M.COLORS = COLORS_SET_1

-- Color schemes of interest
local fav_color_schemes_list = {
  "Catppuccin Macchiato",
  "Whimsy",
  "wilmersdorf",
  "OneHalfDark",
}
M.color_scheme = fav_color_schemes_list[2]

return M
