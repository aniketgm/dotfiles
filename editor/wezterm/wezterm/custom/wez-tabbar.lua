local wz = require("wezterm")

local cl = require("custom.wez-colors")

local M = {}
local C = {}

-- Personal tab bar styling
C.apply_to_config = function(cf)
  cf.tab_bar_style = {
    new_tab = wz.format({
      { Background = { Color = cl.COLORS.ACTIVE_FG } },
      { Foreground = { Color = cl.COLORS.ACTIVE_FG } },
      { Text = cl.SEPARATOR.RIGHT_FILLED },
      { Background = { Color = cl.COLORS.ACTIVE_FG } },
      { Foreground = { Color = cl.COLORS.HOVER_FG } },
      { Text = " + " },
      { Background = { Color = cl.COLORS.NORMAL_BG } },
      { Foreground = { Color = cl.COLORS.ACTIVE_FG } },
      { Text = cl.SEPARATOR.RIGHT_FILLED },
    }),
    new_tab_hover = wz.format({
      { Attribute = { Italic = false } },
      { Attribute = { Intensity = "Bold" } },
      { Background = { Color = cl.COLORS.HOVER_FG } },
      { Foreground = { Color = cl.COLORS.HOVER_BG } },
      { Text = cl.SEPARATOR.RIGHT_FILLED },
      { Background = { Color = cl.COLORS.HOVER_FG } },
      { Foreground = { Color = cl.COLORS.HOVER_BG } },
      { Text = " + " },
      { Background = { Color = cl.COLORS.NORMAL_BG } },
      { Foreground = { Color = cl.COLORS.HOVER_FG } },
      { Text = cl.SEPARATOR.RIGHT_FILLED },
    }),
  }
  cf.colors = {
    tab_bar = {
      background = cl.COLORS.NORMAL_BG,
    },
  }
end

-- Include plugin: tab bar
-- local tabline = wz.plugin.require("https://github.com/aniketgm/tabline.wez")
-- tabline.setup({
--   options = {
--     theme = cl.color_scheme,
--     color_overrides = {
--       tab = {
--         inactive = { fg = "#89b4fa" },
--         active = { fg = cl.COLORS.ACTIVE_FG, bg = cl.COLORS.ACTIVE_BG },
--       },
--     },
--   },
--   sections = {
--     tabline_a = { "" },
--     tabline_b = { "" },
--     tab_active = { "index", { "tab", padding = { left = 0, right = 1 } }, color = {} },
--     tab_inactive = { "index", { "tab", padding = { left = 0, right = 1 } } },
--     tabline_x = {
--       {
--         "datetime",
--         style = "[%a] %b/%d",
--         hour_to_icon = false,
--         icon = { wz.nerdfonts.md_calendar, color = { fg = "#56DEAA" } },
--       },
--     },
--     tabline_y = {
--       "datetime",
--       {
--         "battery",
--         battery_to_icon = {
--           empty = { wz.nerdfonts.md_battery_alert_variant_outline, color = { fg = "#FE3727" } },
--           quarter = { wz.nerdfonts.md_battery_low, color = { fg = "#DE7356" } },
--           half = wz.nerdfonts.md_battery_medium,
--           three_quarters = wz.nerdfonts.md_battery_high,
--           full = { wz.nerdfonts.md_battery, color = { fg = "#73E45A" } },
--         },
--       },
--     },
--     tabline_z = { "workspace" },
--   },
-- })
-- M.tabline = tabline

return C
