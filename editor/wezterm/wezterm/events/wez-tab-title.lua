local wz = require("wezterm")

local cl = require("custom.wez-colors")
local fn = require("custom.wez-functions")

local M = {}

M.setup = function()
  wz.on("format-tab-title", function(tab, tabs, panes, config, hover, max_width)
    panes = panes
    config = config
    max_width = max_width

    local background = cl.COLORS.NORMAL_BG
    local foreground = cl.COLORS.NORMAL_FG
    local title_italic = false

    local is_first = tab.tab_id == tabs[1].tab_id
    local is_last = tab.tab_id == tabs[#tabs].tab_id

    if tab.is_active then
      background = cl.COLORS.ACTIVE_BG
      foreground = cl.COLORS.ACTIVE_FG
      title_italic = true
    elseif hover then
      background = cl.COLORS.HOVER_BG
      foreground = cl.COLORS.HOVER_FG
    end

    local leading_fg = cl.COLORS.NORMAL_FG
    local leading_bg = background
    local trailing_fg = background
    local trailing_bg = cl.COLORS.NORMAL_BG
    local txt_unicode = ""

    if is_first then
      leading_fg = cl.COLORS.ACTIVE_FG
      txt_unicode = cl.SEPARATOR.RIGHT_FILLED
    else
      leading_fg = cl.COLORS.NORMAL_BG
      txt_unicode = cl.SEPARATOR.RIGHT_FILLED
    end

    if is_last then
      trailing_bg = cl.COLORS.HOVER_BG
    else
      trailing_bg = cl.COLORS.NORMAL_BG
    end

    local tab_text = fn.tab_title(tab)

    return {
      { Background = { Color = leading_bg } },
      { Foreground = { Color = leading_fg } },
      { Text = txt_unicode },
      { Attribute = { Italic = title_italic } },
      { Attribute = { Intensity = hover and "Bold" or "Normal" } },
      { Background = { Color = background } },
      { Foreground = { Color = foreground } },
      { Text = " " .. wz.nerdfonts.fa_cube .. " " .. tab_text .. " " },
      { Background = { Color = trailing_bg } },
      { Foreground = { Color = trailing_fg } },
      { Text = cl.SEPARATOR.RIGHT_FILLED },
    }
  end)
end

return M
