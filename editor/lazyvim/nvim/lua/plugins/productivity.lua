return {
  {
    "atiladefreitas/dooing",
    config = function()
      require("dooing").setup({
        window = {
          position = "bottom-right",
          width = 50, -- Width of the floating window
          height = 15, -- Height of the floating window
          border = "rounded", -- Border style
          padding = {
            top = 1,
            bottom = 1,
            left = 5,
            right = 2,
          },
        },
      })
    end,
    keys = {
      { "<leader>td", desc = "Toggle Todo List" },
    },
    opts = {},
  },
  {
    "epwalsh/pomo.nvim",
    version = "*", -- Recommended, use latest release instead of latest commit
    lazy = true,
    cmd = { "TimerStart", "TimerRepeat", "TimerSession" },
    dependencies = { "rcarriga/nvim-notify" },
    opts = {
      notifiers = {
        {
          name = "Default",
          opts = {
            sticky = false,
          },
        },
      },
      sessions = {
        pomo1 = {
          { name = "Work", duration = "25m" },
          { name = "Short Break", duration = "5m" },
          { name = "Work", duration = "25m" },
          { name = "Short Break", duration = "5m" },
          { name = "Work", duration = "25m" },
          { name = "Long Break", duration = "15m" },
        },
        pomo2 = {
          { name = "Work", duration = "25m" },
          { name = "Short Break", duration = "5m" },
          { name = "Work", duration = "25m" },
          { name = "Long Break", duration = "10m" },
        },
      },
    },
    keys = {
      { "<leader>ps", "<cmd>TimerStart 25m Work<cr>", desc = "Timer start" },
      { "<leader>pe", "<cmd>TimerStop<cr>", desc = "Timer stop" },
      { "<leader>pd", "<cmd>TimerShow<cr>", desc = "Display timer" },
      { "<leader>pp1", "<cmd>TimerSession pomo1<cr>", desc = "Session pomo1" },
      { "<leader>pp2", "<cmd>TimerSession pomo2<cr>", desc = "Session pomo2" },
    },
  },
}
