from dooit.api import Todo
from dooit.ui.api import DooitAPI, subscribe
from dooit.ui.api.events import Startup
from dooit.ui.api.widgets import TodoWidget
from dooit_extras.bar_widgets import *
from dooit_extras.formatters import *
from dooit_extras.scripts import *
from rich.style import Style
from rich.text import Text
import requests

# Custom stuff defined in components.py. Pull as and when required
from components import DooitThemeCatppuccin
from components import IconPack

icons = IconPack('Set1').get_icons()

@subscribe(Startup)
def setup_colorscheme(api: DooitAPI, _):
    api.css.set_theme(DooitThemeCatppuccin)


@subscribe(Startup)
def setup_formatters(api: DooitAPI, _):
    fmt = api.formatter
    theme = api.vars.theme

    # ------- WORKSPACES -------
    format = Text(" ({}) ", style=theme.primary).markup
    fmt.workspaces.description.add(description_children_count(format))

    # --------- TODOS ---------
    # status formatter
    fmt.todos.status.add(status_icons(
        completed=icons['status']['completed'],
        pending=icons['status']['pending'],
        overdue=icons['status']['overdue']
    ))

    # urgency formatte
    u_icons = {
        1: icons['urgency']['no_urg'], 2: icons['urgency']['low'],
        3: icons['urgency']['medium'], 4: icons['urgency']['high']
    }
    fmt.todos.urgency.add(urgency_icons(icons=u_icons))

    # due formatter
    fmt.todos.due.add(due_casual_format())
    fmt.todos.due.add(due_icon(
        completed=icons['due']['completed'],
        pending=icons['due']['pending'],
        overdue=icons['due']['overdue']
    ))

    # effort formatter
    fmt.todos.effort.add(effort_icon(icon=icons['effort']))

    # description formatter
    format = Text(" פּ {completed_count}/{total_count}", style=theme.green).markup
    fmt.todos.description.add(todo_description_progress(fmt=format))
    fmt.todos.description.add(description_highlight_tags(fmt=" {}"))
    fmt.todos.description.add(description_strike_completed())


@subscribe(Startup)
def setup_layout(api: DooitAPI, _):
    api.layouts.todo_layout = [
        TodoWidget.status,
        TodoWidget.description,
        TodoWidget.due,
        TodoWidget.effort,
        TodoWidget.urgency
    ]


@subscribe(Startup)
def setup_bar(api: DooitAPI, _):
    theme = api.vars.theme
    widgets = [
        TextBox(api, "   ", bg=theme.magenta),
        Spacer(api, width=1),
        Mode(api, format_normal="  NOR ", format_insert="  INS "),
        Spacer(api, width=0),
        # WorkspaceProgress(api, fmt=" 󰞯 {}% ", bg=theme.secondary),
        StatusIcons(
            api,
            completed_icon=icons['status']['completed'],
            pending_icon=icons['status']['pending'],
            overdue_icon=icons['status']['overdue']
        ),
        Spacer(api, width=1),
        Date(api, fmt=" 󰃰 {} "),
    ]
    api.bar.set(widgets)


def get_random_quote():
    try:
        resp = requests.get("https://zenquotes.io/api/random")
        if resp.status_code == 200:
            data = resp.json()
            return f"{data[0]['q']}\n                                                          -- {data[0]['a']}"
        else:
            return "ZenQuotes empty response"
    except Exception as e:
        raise Exception("ZenQuotes API failed")


@subscribe(Startup)
def setup_dashboard(api: DooitAPI, _):
    theme = api.vars.theme
    ascii_art = r"""
              __________________                    
          /\  \   __           /  /\    /\          
         /  \  \  \         __/  /  \  /  \         
        /    \  \       _____   /    \/    \        
       /  /\  \  \     /    /  /            \       
      /        \  \        /  /      \/      \      
     /          \  \      /  /                \     
    /            \  \    /  /                  \    
   /              \  \  /  /                    \   
  /__            __\  \/  /__                  __\  
    """

    ascii_art = Text(ascii_art, style=theme.magenta)
    ascii_art.highlight_words(["TODOS"], style=theme.red)

    pending = sum([1 for i in Todo.all() if i.is_pending])
    overdue = sum([1 for i in Todo.all() if i.is_overdue])
    all_tasks = sum([1 for i in Todo.all()])

    header = Text(
        get_random_quote(),
        style=Style(color=theme.secondary, bold=True, italic=True),
    )

    items = [
        ascii_art,
        "",
        Text(" Total   Tasks: {}".format(all_tasks), style=theme.orange),
        Text("󰠠 Pending Tasks: {}".format(pending), style=theme.green),
        Text("󰁇 Overdue Tasks: {}".format(overdue), style=theme.red),
        "",
        header,
    ]
    api.dashboard.set(items)


@subscribe(Startup)
def setup_keys(api: DooitAPI, _):
    # Remove existing bindings -- I don't like it
    api.keys.set("c", api.no_op)
    api.keys.set("<ctrl+q>", api.no_op)
    api.keys.set("xx", api.no_op)
    api.keys.set("z", api.no_op)

    # Custom bindings
    api.keys.set("q", api.quit)
    api.keys.set("t", api.toggle_complete)
    api.keys.set("w", toggle_workspaces(api))
    api.keys.set("c", api.remove_node)
    api.keys.set(["h","l"], api.toggle_expand)
