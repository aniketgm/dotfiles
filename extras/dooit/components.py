from dooit.api.theme import DooitThemeBase

# Theme
class DooitThemeCatppuccin(DooitThemeBase):
    _name: str = "dooit-catppuccin"

    # background colors
    background1: str = "#1e1e2e"  # Darkest
    background2: str = "#313244"
    background3: str = "#45475a"  # Lightest

    # foreground colors
    foreground1: str = "#a6adc8"  # Lightest
    foreground2: str = "#bac2de"
    foreground3: str = "#cdd6f4"  # Darkest

    # other colors
    red: str = "#f38ba8"
    orange: str = "#fab387"
    yellow: str = "#f9e2af"
    green: str = "#a6e3a1"
    blue: str = "#89b4fa"
    purple: str = "#b4befe"
    magenta: str = "#f5c2e7"
    cyan: str = "#89dceb"

    # accent colors
    primary: str = purple
    secondary: str = blue


class IconPack:
    def __init__(self, pack_name):
        self.pack = pack_name

    def get_icons(self) -> dict:
        if (self.pack == 'Set1'):
            return {
                "status": { "completed": " ", "pending": " ", "overdue": " " },
                "due": { "completed": "󱫐 ", "pending": "󱫚 ", "overdue": "󱫦 " },
                "urgency": { "high": " ", "medium": " ", "low": " ", "no_urg": "" },
                "effort": " "
            }
        elif (self.pack == 'Set2'):
            return {
                "status": { "completed": " ", "pending": " ", "overdue": " " },
                "due": { "completed": " ", "pending": " ", "overdue": " " },
                "urgency": { "high": " ", "medium": " ", "low": " ", "no_urg": "" },
                "effort": " "
            }
