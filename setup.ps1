#Requires -RunAsAdministrator
#Requires -Version 7.0

$ErrorActionPreference = 'Ignore'

# Write console message. Options: -a -> add new line, -i -> add install msg
function WriteMsg($MsgText, [Switch]$AddEmptyLine ,[Switch]$Install)
{
  If ($AddEmptyLine.IsPresent) { Write-Output "" } # Empty line for readability
  If ($Install.IsPresent) { Write-Output "Installing $MsgText.." }
  Else { Write-Output $MsgText }
}

# Main
If (-Not (Get-Command -Name rotz))
{
  WriteMsg -a "Rotz not found. Installing.."
  WriteMsg "NOTE: If installed was already performed. Ctrl+c from here, check where it's installed and run from there"
  Read-Host "Press <Ctrl+c> to cancel OR <Enter> to continue with the installation.."
  Invoke-RestMethod volllly.github.io/rotz/install.ps1 | Invoke-Expression
  If (@('Y', 'y') -contains (Read-Host "Restart is required. Restart now ?(y/n)")) { shutdown /r /t 10 }
  Else { WriteMsg -a "Running 'setup.ps1' may not function properly without restart." }
}
ElseIf (-Not (Get-Command -Name winget))
{
  WriteMsg -a "Winget not found. Usually it comes by default with Windows 10+ installations."
  WriteMsg "Winget is required. Check out winget-cli on github on how to install it."
}
ElseIf (-Not (Get-Command -Name pwsh))
{
  ((Get-ExecutionPolicy LocalMachine) -ne "RemoteSigned") -And (Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine)
  rotz install essentials/vcredist
  rotz install essentials/pwsh
  rotz install essentials/winterm
  WriteMsg -a "Essential softwares are installed. Restart again for changes to take effect"
  WriteMsg "After restart, re-run the script 'setup.ps1' again to continue with the setup"
  If (@('Y', 'y') -contains (Read-Host "Restart Now ?(y/n)")) { shutdown /r /t 10 }
  Else { WriteMsg -a "Running 'setup.ps1' may not function properly without restart." }
}
ElseIf ($PSVersionTable.PSVersion.Major -ge 7)
{
  ((Get-ExecutionPolicy LocalMachine) -ne "RemoteSigned") -And (Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine)
  rotz install editor/*
  rotz install extras/fonts
  # rotz install extras/scoop/*
  rotz install extras/winget

  # Now link everything
  rotz link essentials/git
  rotz link --link-type hard essentials/pwsh
  rotz link editor/*
  WriteMsg -a "Restart one final time for setup to complete."
  If (@('Y', 'y') -contains (Read-Host "Restart Now ?(y/n)")) { shutdown /r /t 10 }
  Else { WriteMsg -a "Software installed may not function properly without restart." }
}
Else
{
  WriteMsg -a "Now script needs Powershell 7 (pwsh) to run further setup."
  WriteMsg "Open Powershell 7 and re-run the 'setup.ps1' script again."
}
